//Imports
const express = require('express');
const path = require('path');

// Initializations
const app = express();

//settings
app.set('port',4000);
app.set('views',path.join(__dirname,"views"));
app.set('view engine','ejs');
app.engine('html',require('ejs').renderFile);

//statics
app.use(express.static(path.join(__dirname,'public')));

//middlewares

//routes
app.use(require('./routes/index'))


//listening the server
app.listen(app.get('port'),()=>{

    console.log("Server on port ", app.get("port"));

});